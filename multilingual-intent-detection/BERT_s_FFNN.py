import sys
import numpy as np
import torch
from keras.layers import Dense
from keras.models import Sequential
from keras.optimizers import Adam
import tensorflow as tf
import io
import os
import random
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sentence_transformers import SentenceTransformer, util

max_seq = 30 # sequence length
epochs = 100 # number of epochs
batch_size = 100 # batch size

# multilingual sentence embedding models:
pretrained_weights = 'distiluse-base-multilingual-cased-v2'
#pretrained_weights = 'xlm-r-distilroberta-base-paraphrase-v1'
#pretrained_weights = 'xlm-r-bert-base-nli-stsb-mean-tokens'
#pretrained_weights = 'distilbert-multilingual-nli-stsb-quora-ranking'

def createAdamOptimizer(lr=0.001, beta_1=0.9, beta_2=0.999, decay=0, epsilon=None, amsgrad=False):
    opt = Adam(lr=lr, beta_1=beta_1, beta_2=beta_2, epsilon=epsilon, decay=decay, amsgrad=amsgrad)
    return opt

def targets_to_tensor(lines_cls, cls_dict):
    y1 = []
    for cls in lines_cls:
        y1.append(cls_dict[cls])
    y = tf.keras.utils.to_categorical(y1, num_classes=len(cls_dict))

    return y1, torch.tensor(y, dtype=torch.float32)

def create_dict (target_columns):
    if not os.path.exists("models"):
        os.mkdir("models")
        
    cls_dict = {}
    
    for i in range(len(target_columns)):
        cls_dict[target_columns[i]] = i

    f = open("models/map.txt", "w")
    for key in cls_dict:
        f.write(key + "\t" + str(cls_dict[key]) + "\n")
    f.close()
    return cls_dict
    
def read_data (data_file, cls_file):
    f = io.open(data_file, mode="r", encoding="utf-8")
    lines = f.read().split("\n")
    f.close()
    if ("" in lines):
        lines.remove("")

    f = io.open(cls_file, mode="r", encoding="utf-8")
    cls = f.read().split("\n")
    f.close()
    if ("" in cls):
        cls.remove("")

    both = list(zip(lines, cls))
    random.shuffle(both)
    lines, cls = zip(*both)

    return lines, cls

lang_train = sys.argv[1]
lang_test = sys.argv[2]

model = SentenceTransformer(pretrained_weights)

# reads the training data:
sentences, cls_train = read_data ("data/" + lang_train + "/train.txt", "data/train_cls.txt")
target_columns = list(set(cls_train))
cls_dict = create_dict(target_columns)
train_sentence_embeddings = model.encode(sentences)

# reads the testing data: 
sentences, cls_test = read_data ("data/" + lang_test + "/test.txt", "data/test_cls.txt") 
test_sentence_embeddings = model.encode(sentences)

y_train, y_train_t = targets_to_tensor(cls_train, cls_dict)
y_train_t = tf.convert_to_tensor(y_train_t)
y_test, y_test_t = targets_to_tensor(cls_test, cls_dict)
y_test_t = tf.convert_to_tensor(y_test_t)

class_num = y_train_t.shape[1]
embed_dim = train_sentence_embeddings.shape[1]

model= Sequential()
model.add(Dense(units=64, input_shape=(embed_dim,)))
model.add(Dense(class_num, activation='softmax'))
print(model.summary())

optimizer = createAdamOptimizer()
model.compile(optimizer=optimizer,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
model.fit(train_sentence_embeddings, y_train_t, epochs=epochs, batch_size = batch_size, shuffle=True, verbose=2)

model.save("models/model.h5")
#model = keras.models.load_model("models/model.h5")

score = model.evaluate(test_sentence_embeddings, y_test_t, verbose = 0)

predictions = model.predict(test_sentence_embeddings)
predictions = np.asarray(predictions).argmax(axis=-1)

accuracy = accuracy_score(y_test, predictions)
macro_precision = precision_score(y_test, predictions, average='macro', zero_division = 1)
macro_recall = recall_score(y_test, predictions, average='macro', zero_division = 1)
macro_f1 = f1_score(y_test, predictions, average='macro', zero_division = 1)

f = open('test_results.txt', 'a')
f.write("%s\t%s\t%s\t%.3f\t%.3f\t%.3f\t%.3f\n" % ("BERT-s+FFNN", lang_train + "->" + lang_test, pretrained_weights, accuracy, macro_precision, macro_recall, macro_f1))
f.close()
