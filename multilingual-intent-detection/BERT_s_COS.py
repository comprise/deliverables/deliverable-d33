import sys
import numpy as np
import torch
import tensorflow as tf
import io
import random
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sentence_transformers import SentenceTransformer, util

max_seq = 30 # sequence length
epochs = 100 # number of epochs
batch_size = 100 # batch size

# multilingual sentence embedding models:
pretrained_weights = 'distiluse-base-multilingual-cased-v2'
#pretrained_weights = 'xlm-r-distilroberta-base-paraphrase-v1'
#pretrained_weights = 'xlm-r-bert-base-nli-stsb-mean-tokens'
#pretrained_weights = 'distilbert-multilingual-nli-stsb-quora-ranking'
    
def read_data (data_file, cls_file):
    f = io.open(data_file, mode="r", encoding="utf-8")
    lines = f.read().split("\n")
    f.close()
    if ("" in lines):
        lines.remove("")

    f = io.open(cls_file, mode="r", encoding="utf-8")
    cls = f.read().split("\n")
    f.close()
    if ("" in cls):
        cls.remove("")

    both = list(zip(lines, cls))
    random.shuffle(both)
    lines, cls = zip(*both)

    return lines, cls

lang_train = sys.argv[1]
lang_test = sys.argv[2]

model = SentenceTransformer(pretrained_weights)

# reads the training data:
sentences, cls_train = read_data ("data/" + lang_train + "/train.txt", "data/train_cls.txt")
train_sentence_embeddings = model.encode(sentences)

# reads the testing data: 
sentences, cls_test = read_data ("data/" + lang_test + "/test.txt", "data/test_cls.txt") 
test_sentence_embeddings = model.encode(sentences)

predictions = []
for vect_test, cl_test in zip(test_sentence_embeddings, cls_test):
    sim_val = []
    for vect_train, cl_train in zip(train_sentence_embeddings, cls_train):
        cos_scores = util.pytorch_cos_sim(vect_test, vect_train)[0]
        cos_scores = cos_scores.cpu()
        sim_val.append(cos_scores)
        
    sim_val_cls = cls_train[:]
    sim_val, sim_val_cls = (list(t) for t in zip(*sorted(zip(sim_val, sim_val_cls))))
    sim_val_cls.reverse()
    predictions.append(sim_val_cls[0])

accuracy = accuracy_score(cls_test, predictions)
macro_precision = precision_score(cls_test, predictions, average='macro', zero_division = 1)
macro_recall = recall_score(cls_test, predictions, average='macro', zero_division = 1)
macro_f1 = f1_score(cls_test, predictions, average='macro', zero_division = 1)

f = open('test_results.txt', 'a')
f.write("%s\t%s\t%s\t%.3f\t%.3f\t%.3f\t%.3f\n" % ("BERT-s+COS", lang_train + "->" + lang_test, pretrained_weights, accuracy, macro_precision, macro_recall, macro_f1))
f.close()
